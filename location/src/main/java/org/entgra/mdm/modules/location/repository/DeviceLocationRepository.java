/* Copyright (c) 2020, Entgra (Pvt) Ltd. (http://www.entgra.io) All Rights Reserved.
 *
 * Entgra (Pvt) Ltd. licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.entgra.mdm.modules.location.repository;

import org.entgra.mdm.modules.location.beans.dto.LocationDTO;
import org.entgra.mdm.modules.location.beans.entity.Location;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface DeviceLocationRepository extends CrudRepository<Location, Integer>  {

    @Query(value = "SELECT "
            + "latitude, "
            + "longitude, "
            + "street1, "
            + "street2, "
            + "city, "
            + "zip, "
            + "street, "
            + "country, "
            + "geoHash, "
            + "updateTimestamp, "
            + "altitude, "
            + "speed, "
            + "bearing, "
            + "distance "
            + "FROM location "
            + "WHERE enrollmentId = :enrollmentId AND "
            + "tenantId = :tenantId",
            nativeQuery = true)
    LocationDTO findDeviceLocation(int enrollmentId, int tenantId);
}
