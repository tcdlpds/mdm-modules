/* Copyright (c) 2020, Entgra (Pvt) Ltd. (http://www.entgra.io) All Rights Reserved.
 *
 * Entgra (Pvt) Ltd. licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.entgra.mdm.modules.location.service;

import com.google.protobuf.Timestamp;
import io.grpc.stub.StreamObserver;
import org.entgra.mdm.modules.location.Device;
import org.entgra.mdm.modules.location.DeviceLocation;
import org.entgra.mdm.modules.location.beans.dto.LocationDTO;
import org.entgra.mdm.modules.location.repository.DeviceLocationRepository;
import org.lognet.springboot.grpc.GRpcService;
import org.entgra.mdm.modules.location.DeviceLocationServiceGrpc;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.concurrent.TimeUnit;

@GRpcService
public class DeviceLocationService extends DeviceLocationServiceGrpc.DeviceLocationServiceImplBase {

    @Autowired
    private DeviceLocationRepository deviceLocationRepository;

    @Override
    public void getDeviceLocation(Device request,
            StreamObserver<DeviceLocation> responseObserver) {

        LocationDTO locationDTO = deviceLocationRepository
                .findDeviceLocation(request.getEnrollmentId(), request.getTenantId());
        DeviceLocation deviceLocation = null;
        if (locationDTO != null) {
            deviceLocation = DeviceLocation.newBuilder()
                    .setBearing(locationDTO.getBearing())
                    .setCity(locationDTO.getCity())
                    .setAltitude(locationDTO.getAltitude())
                    .setCountry(locationDTO.getCountry())
                    .setLatitude(locationDTO.getLatitude())
                    .setLongitude(locationDTO.getLongitude())
                    .setGeoHash(locationDTO.getGeoHash())
                    .setSpeed(locationDTO.getSpeed())
                    .setStreet1(locationDTO.getStreet1())
                    .setStreet2(locationDTO.getStreet2())
                    .setUpdateTimestamp(Timestamp.newBuilder().setSeconds(
                            TimeUnit.MILLISECONDS.toSeconds(locationDTO.getUpdateTimestamp().getTime()))
                            .setNanos(locationDTO.getUpdateTimestamp().getNanos()).build())
                    .setZip(locationDTO.getZip())
                    .setDistance(locationDTO.getDistance())
                    .setSpeed(locationDTO.getSpeed()).build();
        }
        responseObserver.onNext(deviceLocation);
        responseObserver.onCompleted();
    }
}
